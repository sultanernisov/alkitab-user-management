<div class="c-edit-profile" data-profile-edit>

  <div class="c-edit-profile--menu">
    <button
      class="c-edit-profile--menu-item active" 
      data-edit-menu-item="avatar"
    >
      <?= __( 'Avatar', 'alkitab-users' ); ?>
    </button>

    <button
      class="c-edit-profile--menu-item" 
      data-edit-menu-item="info"
    >
      <?= __( 'Basic information', 'alkitab-users' ); ?>
    </button>
    
    <button
      class="c-edit-profile--menu-item" 
      data-edit-menu-item="tutor"
    >
      <?= __( 'Tutor', 'alkitab-users' ); ?>
    </button>
  </div>

  <!-- forms -->
  <div class="c-edit-profile--forms">

    <!-- avatar -->
    <div class="c-edit-profile--form active" data-edit-form="avatar">
      <?php
        if ( shortcode_exists( 'avatar_upload' ) ) {
          echo do_shortcode('[avatar_upload]');
        }
      ?>
    </div>

    <div class="c-edit-profile--form" data-edit-form="info">
      <h3>
        <?= __( 'Basic information', 'alkitab-users' ); ?>
      </h3>

      <div class="c-form--field">
        <label
          class="c-form--label"
          for="firstName"
        >
          <?= __( 'First name', 'alkitab-users' ); ?>
        </label>
        <input
          class="c-form--input"
          id="firstName"
          type="text"
          name="first_name"
          autocomplete="off"
          value="<?= $env['first_name']; ?>"
        >
      </div>

      <div class="c-form--field">
        <label
          class="c-form--label"
          for="lastName"
        >
          <?= __( 'Last name', 'alkitab-users' ); ?>
        </label>
        <input
          class="c-form--input"
          id="lastName"
          type="text"
          name="last_name"
          autocomplete="off"
          value="<?= $env['last_name']; ?>"
        >
      </div>

      <div class="c-form--field">
        <label
          class="c-form--label"
          for="email"
        >
          <?= __( 'Email address', 'alkitab-users' ); ?>
        </label>
        <input
          class="c-form--input"
          id="email"
          type="email"
          name="email"
          autocomplete="off"
          value="<?= $env['email']; ?>"
        >
      </div>

      <div class="c-form--field">
        <label for="countrySelect" class="c-form--label">
          <?= __( 'Country', 'alkitab-users' ); ?>
        </label>
        <input
          class="c-form--input"
          id="countrySelect"
          name="country"
          value="<?= $env['country']; ?>"
        >
      </div>

      <button
        class="c-form--submit"
        data-edit-submit="info"
      >
        <?= __( 'Submit', 'alkitab-users' ); ?>
      </button>

    </div>

    <div class="c-edit-profile--form" data-edit-form="tutor">
      <h3><?= __( 'Tutor', 'alkitab-users' ); ?></h3>

      <div class="c-tutor-container">

        <?php
          $tutors = get_users( array( 'role' => 'tutor' ) );

          foreach ( $tutors as $tutor ) {
            $avatar_url = get_avatar_url( $tutor->ID );
        ?>
          <div
            class="c-tutor <?= ($env['tutor']->ID == $tutor->ID) ? 'selected' : ''; ?>"
            data-tutor="<?= $tutor->ID; ?>"
          >
            <img src="<?= $avatar_url; ?>" class="c-tutor--avatar">
            <p><?= $tutor->display_name; ?></p>
            <div class="c-tutor--languages">
              <?php
                foreach ( get_user_meta( $tutor->ID, 'tutor_languages', true ) as $language ):
                  if ( is_array( $language ) ):
              ?>
                  <span class='c-tutor--language'>
                    <img src="<?= $language['flag_url']; ?>">
                    <span><?= $language['label']; ?></span>
                  </span>
              <?php endif; endforeach; ?>
            </div>
          </div>
        <?php
          }
        ?>
      </div>
      
      <button
        data-edit-submit="tutor"
        class="c-form--submit"
      >
        <?= __( 'Submit', 'alkitab-users' ); ?>
      </button>
    </div>
  </div>
</div>