<div class="c-lostpassword">
  <h1><?= __( 'Forgot your password?', 'alkitab-users' ); ?></h1>
  <p><?=
    __( 'Enter your email address and we will send you a link you can use to pick a new password', 'alkitab-users' );
  ?></p>

  <?php if ( count( $env['errors'] ) > 0 ): ?>
    <div class="c-form--errors" data-error-message>
      <i class="c-form--errors--close" data-error-close data-feather="x-circle"></i>
      <?php foreach ( $env['errors'] as $error ): ?>
        <p><?= $error; ?></p>
      <?php endforeach ?>
    </div>
    <?php endif; ?>

  <form class="c-lostpassword-form" action="<?= wp_lostpassword_url(); ?>" method="POST">
    <div class="c-form--field">
      <label class="c-form--label" for="username">
        <?= __( 'Email or username', 'alkitab-users' ); ?>
      </label>
      <input autocomplete="off" class="c-form--input" type="text" name="user_login" id="username">
    </div>

    <input class="c-form--submit" type="submit" value="<?= __( 'Submit', 'alkitab-users' ); ?>">
  </form>
</div>