<?php
  function tutor_language_form( $user ) {
?>
  <hr>
  <h2><?= __( "Select tutor's languages", 'alkitab-users' ); ?></h2>
  <table class="form-table">
    <tr>
      <th><label for="languages"><?= __( 'Languages', 'alkitab-users' ); ?></label></th>
    </tr>
    <td>
      <?php
        foreach ( \AlkitabUsers\API\Languages::get_languages() as $lang ) {
          $checkbox = in_array( $lang['code'], array_keys(get_user_meta($user->ID, 'tutor_languages', true)) ) ? "checked" : "";
          $code = $lang['code'];
      ?>

          <label>
            <input type='hidden' name='flag_url[<?= $code; ?>]' value='<?= $lang['flag_url']; ?>'>
            <input type='hidden' name='label[<?= $code; ?>]' value='<?= $lang['label']; ?>' >
            <input type='checkbox' name='selected[]' value='<?= $code; ?>' <?= $checkbox; ?>>
            <?= $lang['label']?>
          </label><br>
      <?php
        }
      ?>
    </td>
  </table>
  <hr>
<?php
}