<div class="c-resetpassword">
  <h1><?= __( 'Pick a new password', 'alkitab-users' ); ?></h1>

  <?php if ( count( $env['errors'] ) > 0 ): ?>
    <div class="c-form--errors" data-error-message>
    <i class="c-form--errors--close" data-feather="x-circle" data-error-close></i>
    <?php foreach ( $env['errors'] as $error ): ?>
        <p><?= $error; ?></p>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>

  <form
    name="resetpassform"
    action="<?= site_url( 'wp-login.php?action=rp' ) ?>"
    method="POST"
    autocomplete="off"
  >
    <input id="user_login" autocomplete="off" type="hidden" name="rp_login" value="<?= esc_attr( $env['login'] ); ?>">
    <input type="hidden" name="rp_key" value="<?= esc_attr( $env['key'] ); ?>">

    <div class="c-form--field">
      <label class="c-form--label" for="pass1"><?= __( 'new password', 'alkitab-users' ); ?></label>
      <input class="c-form--input" name="pass1" type="password" id="pass1" autocomplete="off">
    </div>

    <div class="c-form--field">
      <label class="c-form--label" for="pass2"><?= __( 'repeat new password', 'alkitab-users' ); ?></label>
      <input class="c-form--input" name="pass2" type="password" id="pass2" autocomplete="off">
    </div>

    <input class="c-form--submit" type="submit" value="<?= __( 'Reset password', 'alkitab-users' ); ?>">
  </form>
</div>