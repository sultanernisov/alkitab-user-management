import $ from 'jquery';
import './vendor/countrySelect';
import MultistepForm from './components/MultistepForm';
import EditProfile from './components/EditProfile';

$(document).ready(() => {
  let countrySelect = $('#countrySelect');
  if (countrySelect) {
    countrySelect.countrySelect();
  }


  const form = new MultistepForm(document.querySelector('[data-multistep-form]'));
  form.init();

  const editProfile = new EditProfile(document.querySelector('[data-profile-edit]'));
  editProfile.init();
});