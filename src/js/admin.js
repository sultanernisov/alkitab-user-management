import $ from 'jquery';

$(document).ready(() => {
  let imageManager = document.querySelector('#alkitab_image_manager');

  if (imageManager) {
    let imageFrame;
    imageFrame = wp.media({
      title: 'Select Media',
      multiple: false,
      library: {
        type: 'image',
      }
    });

    imageFrame.on('close', () => {
      let selection = imageFrame.state().get('selection');
      let galleryIds = [];
      selection.each(attachment => {
        galleryIds.push(attachment.id);
      });
      $('input#alkitab_login_image').val(galleryIds[0]);
      refreshImage(galleryIds[0]);
    });


    imageManager.addEventListener('click', (e) => {
      e.preventDefault();
      if (imageFrame) {
        imageFrame.open();
      }
    });
  }
});

function refreshImage(id) {
  $.ajax({
    method: 'get',
    url: alkitabUsers.registerUrl,
    data: {
      action: 'alkitab_login_image',
      id: id,
    },
    success: (response) => {
      if (response.image) {
        $('#alkitab_login_image_preview').replaceWith( response.image );
      }
    },
    error: (err) => {
      console.log(err);
    }
  });
}