export default (size = '') => {
  let container = document.createElement('div');
  container.className = 'row-center';
  let spinner = document.createElement('div');
  spinner.className = `la-ball-clip-rotate aqua-blue ${size}`;
  spinner.appendChild(document.createElement('div'));
  container.appendChild(spinner);
  return container;
};