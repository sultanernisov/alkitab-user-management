import $ from 'jquery';
import Spinner from './Spinner';

export default class EditProfile {
  constructor(element) {
    if (!element) return;
    this.page = element;
    this.menuItems = this.page.querySelectorAll('[data-edit-menu-item]');
    this.forms = this.page.querySelectorAll('[data-edit-form]');
    this.submits = this.page.querySelectorAll('[data-edit-submit]');
    this.tutors = this.page.querySelectorAll('[data-tutor]');

    if (this.page.querySelector('.c-tutor.selected')) {
      this.tutor = this.page.querySelector('.c-tutor.selected');
    }

    this.setActiveForm = this.setActiveForm.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onTutorSelect = this.onTutorSelect.bind(this);
  }

  init() {
    if (!this.page) return;

    this.menuItems.forEach(item => {
      item.addEventListener('click', this.setActiveForm);
    });

    this.submits.forEach(submit => {
      submit.addEventListener('click', this.onSubmit);
    });

    this.tutors.forEach(tutor => {
      tutor.addEventListener('click', this.onTutorSelect);
    });
  }

  setActiveForm(e) {
    let show = e.target.dataset.editMenuItem;
    this.menuItems.forEach(item => {
      if (item.dataset.editMenuItem === show) {
        item.classList.add('active');
      } else {
        item.classList.remove('active');
      }
    });

    this.forms.forEach(form => {
      if (form.dataset.editForm === show) {
        form.classList.add('active');
      } else {
        form.classList.remove('active');
      }
    });
  }

  onSubmit(e) {
    let settings = e.target.dataset.editSubmit;
    let spinner = Spinner();
    e.target.innerHTML = '';
    e.target.appendChild(spinner);
    e.target.style.backgroundColor = 'white';

    if (settings === 'tutor') {
      $.post({
        url: alkitabUsers.registerUrl,
        data: {
          action: 'edit_profile',
          form: 'tutor',
          tutor: this.tutor
        },
        success: (response) => {
          if (response.update) {
            e.target.innerHTML = alkitabUsers.updated;
            e.target.style.backgroundColor = 'lightgreen';
            setTimeout(() => {
              e.target.innerHTML = alkitabUsers.submit;
              e.target.style.backgroundColor = '';
            }, 2000);
          } else {
            e.target.innerHTML = alkitabUsers.submit;
            e.target.style.backgroundColor = '';
          }
        },
        error: (err) => {
          console.log(err);
        }
      });
    }
    if (settings === 'info') {
      let infoForm = null;
      this.forms.forEach(form => {
        if (form.dataset.editForm === settings) {
          infoForm = form;
        }
      });

      let inputs = infoForm.querySelectorAll('input');
      let values = {};
      inputs.forEach(input => {
        values[input.name] = input.value;
      });

      $.post({
        url: alkitabUsers.registerUrl,
        data: {
          ...values,
          form: 'info',
          action: 'edit_profile'
        },
        success: (response) => {
          if (response.update) {
            e.target.innerHTML = alkitabUsers.updated;
            e.target.style.backgroundColor = 'lightgreen';
            setTimeout(() => {
              e.target.innerHTML = alkitabUsers.submit;
              e.target.style.backgroundColor = '';
            }, 2000);
          } else {
            e.target.innerHTML = alkitabUsers.submit;
            e.target.style.backgroundColor = '';
          }
        },
        error: (err) => {
          console.log(err);
        }
      });
    }
  }

  onTutorSelect(e) {
    let tutor = e.currentTarget.dataset.tutor;
    this.tutor = tutor;
    this.tutors.forEach(t => {
      if (t.dataset.tutor === tutor) {
        t.classList.add('selected');
      } else {
        t.classList.remove('selected');
      }
    });
  }
}