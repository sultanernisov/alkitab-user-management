import $ from 'jquery';
import feather from 'feather-icons';

export default class MultistepForm {
  constructor(form) {
    if (!form) return;
    this.form = form;
    this.navButtons = this.form.querySelectorAll('[data-nav-button]');
    this.steps = this.form.querySelectorAll('[data-step]');
    this.bullets = this.form.querySelectorAll('[data-bullet]');
    this.submit = this.form.querySelector('[data-submit]');

    this.moveEvent = new CustomEvent('registerFormMove', {
      detail: { form: this },
      cancelable: true
    });

    this.currentStep = 0;
    this.tutor = null;
    this.tutors = this.form.querySelectorAll('[data-tutor]');

    this.move = this.move.bind(this);
    this.next = this.next.bind(this);
    this.prev = this.prev.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onMove = this.onMove.bind(this);
    this.onTutorClick = this.onTutorClick.bind(this);
    this.invokeErrorMessages = this.invokeErrorMessages.bind(this);
  }

  init() {
    if (this.form) {
      this.navButtons.forEach(button => {
        let dir = button.dataset.navButton;
        button.addEventListener('click', this[dir]);
        this[`${dir}Button`] = button;
      });

      this.tutors.forEach(tutor => {
        tutor.addEventListener('click', this.onTutorClick);
      });

      this.form.addEventListener('registerFormMove', this.onMove);
      this.submit.addEventListener('click', this.onSubmit);
    }
  }

  next() {
    if ((this.currentStep + 1) < this.steps.length) {
      this.move(this.currentStep + 1);
    }
  }

  prev() {
    if (this.currentStep > 0) {
      this.move(this.currentStep - 1);
    }
  }

  move(step) {
    this.currentStep = step;
    this.form.dispatchEvent(this.moveEvent);
  }

  onSubmit(e) {
    e.preventDefault();
    let inputs = this.form.querySelectorAll('.c-form--input');
    let postData = {};
    inputs.forEach(input => {
      postData[input.name] = input.value;
    });

    postData.tutor = this.tutor;
    let submitText = this.submit.querySelector('.c-register-form--submit-text');
    let spinner = this.submit.querySelector('.la-ball-clip-rotate');

    submitText.classList.add('hidden');
    spinner.classList.remove('hidden');

    $.post({
      url: alkitabUsers.registerUrl,
      method: 'POST',
      data: {
        ...postData,
        action: 'register',
      },
      success: (response) => {
        if (!response.error) {
          window.location.replace(response.redirect);
        } else {
          submitText.classList.remove('hidden');
          spinner.classList.add('hidden');
          this.invokeErrorMessages(response);
        }
      }
    });
  }

  onMove(e) {
    if (this.currentStep === (this.steps.length - 1)) {
      this.nextButton.classList.add('hidden');
      this.submit.classList.add('visible');
    } else {
      this.nextButton.classList.remove('hidden');
      this.submit.classList.remove('visible');
    }

    if (this.currentStep > 0) {
      this.prevButton.classList.remove('nonvisible');
    } else {
      this.prevButton.classList.add('nonvisible');
    }

    this.steps.forEach(step => {
      if (step.dataset.step == this.currentStep) {
        step.removeAttribute('disabled');
      }

      step.style.transform = `translateX(-${this.currentStep * 100}%)`;
    });
    
    this.bullets.forEach(bullet => {
      if (parseInt(bullet.dataset.bullet) === this.currentStep) {
        bullet.classList.add('active');
      } else {
        bullet.classList.remove('active');
      }
    });
  }

  onTutorClick(e) {
    let tutor = e.currentTarget;

    this.tutors.forEach(t => {
      if (t.dataset.tutor == tutor.dataset.tutor) {
        t.classList.add('selected');
        this.tutor = parseInt(tutor.dataset.tutor);
      } else {
        t.classList.remove('selected');
      }
    });
  }

  invokeErrorMessages(response) {
    let focusElement = this.form.querySelector(`[name="${response.focus}"]`);
    let stage = focusElement.closest('[data-step]');
    let errorElement = this.getErrorElement(response, () => focusElement.focus());
    this.move(parseInt(stage.dataset.step));
    stage.insertBefore(errorElement, stage.firstElementChild.nextSibling);
  }

  getErrorElement(response, onClose) {
    let container = document.createElement('div');
    container.classList.add('c-form--errors');
    let closeButton = document.createElement('span');
    closeButton.classList.add('c-form--errors--close');
    closeButton.innerHTML = feather.icons['x-circle'].toSvg();
    closeButton.addEventListener('click', (e) => {
      $(container).animate({
        height: '0px',
        padding: 0,
      }, 200, () => {
        container.remove();
        onClose();
      });
    }, { once: true });

    container.appendChild(closeButton);

    response.message.forEach(m => {
      let message = document.createElement('p');
      message.innerHTML = m;
      container.appendChild(message);
    });

    return container;
  }
}