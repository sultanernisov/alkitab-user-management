<?php

namespace AlkitabUsers\Shortcodes;
use \AlkitabUsers\Base\RenderComponent;

class Profile extends RenderComponent {

  public function register() {
    add_shortcode( 'alkitab-user-profile', array( $this, 'render' ) );
    add_action( 'wp_ajax_edit_profile', array( $this, 'ajax' ) );
  }

  public function render() {
    $user = wp_get_current_user();
    $user_id = $user->ID;
    $user_meta = get_user_meta( $user_id, 'alkitab_user', true );
    $user_courses = ld_get_mycourses( $user->ID );
    $user_courses_completed = array_filter( $user_courses, function($course_id) use ($user_id) {
      return learndash_course_status( $course_id, $user_id, true ) == 'completed';
    });

    if ( !$user_meta ) $user_meta = array();

    $attributes = [
      'id' => $user_id,
      'username' => $user->user_login,
      'display_name' => $user->display_name,
      'first_name' => $user->first_name,
      'last_name' => $user->last_name,
      'avatar_url' => get_avatar_url( $user_id ),
      'birth_date' => isset($user_meta['birth_date']) ? $user_meta['birth_date'] : '',
      'country' => isset($user_meta['country']) ? $user_meta['country'] : '',
      'language' => isset($user_meta['language']) ? $user_meta['language'] : '',
      'tutor' => isset( $user_meta['tutor'] ) ? $this->get_tutor( $user_meta['tutor'] ) : '',
      'email' => $user->user_email,
      'courses_count' => count( $user_courses ),
      'courses_completed_count' => count( $user_courses_completed ),
      'certificates_count' => learndash_get_certificate_count( $user_id )
    ];

    if ( isset( $_GET['action'] ) && $_GET['action'] == 'edit' ) {
      return $this->get_template_html( 'edit_profile', $attributes );
    }

    return $this->get_template_html( 'profile', $attributes );
  }

  private function get_tutor( $tutor_id ) {
    $tutor = get_userdata( $tutor_id );
    return $tutor;
  }

  public function ajax() {
    if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {

      $user = wp_get_current_user();
      $user_meta = get_user_meta( $user->ID, 'alkitab_user', true );

      if ( isset( $_POST['form'] ) && $_POST['form'] == 'tutor' ) {
        $tutor = $_POST['tutor'];

        $user_meta['tutor'] = $tutor;

        $update = update_user_meta( $user->ID, 'alkitab_user', $user_meta );
        wp_send_json(array(
          'update' => $update,
          'result' => $user_meta
        ));
      }

      if ( isset( $_POST['form'] ) && $_POST['form'] == 'info' ) {
        $email = $_POST['email'];
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];

        $language = $_POST['language'];
        $country = $_POST['country'];

        if ( $email ) {
          $user->data->user_email = $email;
        }

        if ( $first_name ) {
          $user->data->first_name = $first_name;
          $user->data->display_name = $first_name . " " .  $user->data->last_name;
        }

        if ( $last_name ) {
          $user->data->last_name = $last_name;
          $user->data->display_name = $user->data->first_name . " " . $last_name;
        }

        if ( $country ) {
          $user_meta['country'] = $country;
        }

        $updated = wp_update_user( $user );

        // custom meta information updated?
        $alkitab_updated = update_user_meta( $user->ID, 'alkitab_user', $user_meta );

        wp_send_json([
          'update' => $updated || $alkitab_updated,
          'result' => array_merge( $user_meta, $user )
        ]);

      }
    }
  }

}