<?php

namespace AlkitabUsers\Shortcodes;
use \AlkitabUsers\Base\AuthComponent;

class LostPassword extends AuthComponent {
  private $lost_password_page;

  public function __construct() {
    $this->lost_password_page = get_option( 'alkitab_lost_password_page' );
  }

  public function register() {
    add_shortcode( 'alkitab-lost-password', array( $this, 'render' ) );
    add_action( 'login_form_lostpassword', array( $this, 'redirect_to_custom_lostpassword' ) );
    add_action( 'login_form_lostpassword', array( $this, 'do_password_lost' ) );
    add_filter( 'retrieve_password_message', array( $this, 'replace_retrieve_password_message' ), 10, 4 );
  }

  public function render( $attributes, $content = null ) {
    $default_atts = array();
    $attributes = shortcode_atts( $default_atts, $attributes );

    if ( is_user_logged_in() ) {
      return __( 'You are already signed in', 'alkitab-users' );
    }

    $attributes['errors'] = array();
    if ( isset( $_REQUEST['errors'] ) ) {
      $error_codes = explode( ',', $_REQUEST['errors'] );

      foreach ( $error_codes as $error_code ) {
        $attributes['errors'][] = $this->get_error_message( $error_code );
      }
    }
    
    return $this->get_template_html( 'lost_password', $attributes );
  }

  private function get_error_message( $error_code ) {
    switch ( $error_code ) {
      case 'empty_username':
        return __( 'You need to enter your email/username address to continue.', 'alkitab-users' );

      case 'invalid_email':
      case 'invalidcombo':
        return __( 'There are no users registered with this email address.', 'alkitab-users' );
    }
  }

  public function do_password_lost() {
    if ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {
      $errors = retrieve_password();

      if ( is_wp_error( $errors ) && isset( $this->lost_password_page ) ) {
        $redirect_url = get_page_link( $this->lost_password_page );
        $redirect_url = add_query_arg( 'errors', join( ',', $errors->get_error_codes() ), $redirect_url );
      } else if ( get_option( 'alkitab_login_page' ) ) {
        $redirect_url = get_page_link( get_option( 'alkitab_login_page' ) );
        $redirect_url = add_query_arg( 'checkmail', 'confirm', $redirect_url );
      } else {
        $redirect_url = wp_login_url();
      }

      wp_redirect( $redirect_url );
      exit;
    }
  }

  public function replace_retrieve_password_message( $message, $key, $user_login, $user_data ) {
    $msg  = __( 'Hello!', 'alkitab-users' ) . "\r\n\r\n";
    $msg .= sprintf( __( 'You asked us to reset your password for your account using the email address %s.', 'alkitab-users' ), $user_login ) . "\r\n\r\n";
    $msg .= __( "If this was a mistake, or you didn't ask for a password reset, just ignore this email and nothing will happen.", 'alkitab-users' ) . "\r\n\r\n";
    $msg .= __( 'To reset your password, visit the following address:', 'alkitab-users' ) . "\r\n\r\n";
    $msg .= site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user_login ), 'login' ) . "\r\n\r\n";
    $msg .= __( 'Thanks!', 'alkitab-users' ) . "\r\n";
 
    return $msg;
  }

  public function redirect_to_custom_lostpassword() {
    if ( 'GET' == $_SERVER['REQUEST_METHOD'] ) {
      if ( is_user_logged_in() ) {
        $this->redirect_logged_in_user();
        exit;
      }

      if ( isset( $this->lost_password_page ) ) {
        wp_redirect( get_page_link( $this->lost_password_page ) );
      }
      exit;
    }
  }
}