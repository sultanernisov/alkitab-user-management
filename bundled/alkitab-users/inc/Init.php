<?php

namespace AlkitabUsers;

final class Init {

  public static function get_services() {
    return array(
      Base\Roles::class,
      Base\Enqueue::class,
      Base\Ajax::class,
      Shortcodes\Login::class,
      Shortcodes\Register::class,
      Shortcodes\LostPassword::class,
      Shortcodes\ResetPassword::class,
      Shortcodes\Profile::class,
      Pages\Admin::class,
    );
  }

  public static function register_services() {
    foreach ( self::get_services() as $class ) {
      $service = self::instanciate( $class );

      if ( method_exists( $service, 'register' ) ) {
        $service->register();
      }
    }
  }

  private static function instanciate( $class ) {
    return new $class;
  }

}