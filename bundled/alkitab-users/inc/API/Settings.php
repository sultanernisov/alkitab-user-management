<?php

namespace AlkitabUsers\API;

class Settings {

  public $admin_pages = array();
  public $admin_subpages = array();
  public $admin_settings = array();
  public $admin_sections = array();
  public $admin_fields = array();

  public function register() {
    if ( !empty( $this->admin_pages ) ) {
      add_action( 'admin_menu', array( $this, 'add_admin_menu' ) );
    }

    if ( !empty( $this->admin_settings ) ) {
      add_action( 'admin_init', array( $this, 'register_custom_fields' ) );
    }
  }

  public function add_admin_menu() {
    foreach ( $this->admin_pages as $page ) {
      add_menu_page( $page['title'], $page['menu_title'], $page['capability'], $page['menu_slug'], $page['callback'], $page['icon_url'], $page['position'] );
    }

    foreach ( $this->admin_subpages as $subpage ) {
      add_submenu_page( $subpage['parent_slug'], $subpage['title'], $subpage['menu_title'], $subpage['capability'], $subpage['menu_slug'], $subpage['callback'] );
    }
  }

  public function set_pages( $pages ) {
    $this->admin_pages = $pages;
    return $this;
  }

  public function with_subpage( $title = NULL ) {
    if ( empty( $this->admin_pages ) ) return;

    $admin_page = $this->admin_pages[0];
    $subpage = array(
      'parent_slug' => $admin_page['menu_slug'],
      'menu_title' => ($title) ? $title : $admin_page['menu_title'],
      'title' => $admin_page['title'],
      'capability' => $admin_page['capability'],
      'menu_slug' => $admin_page['menu_slug'],
      'callback' => $admin_page['callback'],
    );

    $this->admin_subpages = array( $subpage );
    return $this;
  }

  public function set_subpages( $subpages ) {
    $this->admin_subpages = array_merge( $this->admin_subpages, $subpages );
    return $this;
  }

  public function set_settings( $settings ) {
    $this->admin_settings = $settings;
    return $this;
  }

  public function set_sections( $sections ) {
    $this->admin_sections = $sections;
    return $this;
  }

  public function set_fields( $fields ) {
    $this->admin_fields = $fields;
    return $this;
  }

  public function register_custom_fields() {
    // register setting
    foreach ( $this->admin_settings as $setting ) {
      $setting_args = ( isset( $setting['args'] ) ) ? $setting['args'] : '';
      register_setting( $setting['option_group'], $setting['option_name'], $setting_args );
    }
    // add settings section
    foreach ( $this->admin_sections as $section ) {
      $section_callback = ( isset( $section['callback'] ) ) ? $section['callback'] : '';
      add_settings_section( $section['id'], $section['title'], $section_callback, $section['page'] );
    }

    // add settings field
    foreach ($this->admin_fields as $field) {
      $field_callback = ( isset( $field['callback'] ) ) ? $field['callback'] : '';
      $field_args = ( isset( $field['args'] ) ) ? $field['args'] : '';
      add_settings_field( $field['id'], $field['title'], $field_callback, $field['page'], $field['section'], $field_args );
    }    
  }

}