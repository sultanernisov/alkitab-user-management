<?php

namespace AlkitabUsers\API;
use \AlkitabUsers\Base\RenderComponent;

class Callbacks extends RenderComponent {

  public function alkitab_users() {
    echo $this->get_template_html( 'admin_page' );
  }

  // sections
  public function auth_pages() {
    echo "auth pages";
  }

  public function login_page_settings() {
    echo "login page settings";
  }

  public function register_page_settings() {
    echo "register page settings";
  }

  // fields
  public function login_page_field() {
    $selected = get_option( 'alkitab_login_page' );
    $args = array(
      'name' => 'alkitab_login_page',
      'show_option_none' => 'None',
      'selected' => $selected
    );

    wp_dropdown_pages( $args );
  }

  public function register_page_field() {
    $selected = get_option( 'alkitab_register_page' );
    $args = array(
      'name' => 'alkitab_register_page',
      'show_option_none' => 'None',
      'selected' => $selected
    );

    wp_dropdown_pages( $args );
  }

  public function lost_password_field() {
    $selected = get_option( 'alkitab_lost_password_page' );
    $args = array(
      'name' => 'alkitab_lost_password_page',
      'show_option_none' => 'None',
      'selected' => $selected
    );

    wp_dropdown_pages( $args );
  }

  public function reset_password_field() {
    $selected = get_option( 'alkitab_reset_password_page' );
    $args = array(
      'name' => 'alkitab_reset_password_page',
      'show_option_none' => 'None',
      'selected' => $selected
    );

    wp_dropdown_pages( $args );
  }

  public function redirect_after_login_field() {
    $selected = get_option( 'alkitab_redirect_on_login' );
    $args = array(
      'name' => 'alkitab_redirect_on_login',
      'show_option_none' => 'None',
      'selected' => $selected
    );

    wp_dropdown_pages( $args );
  }

  public function redirect_after_register_field() {
    $selected = get_option( 'alkitab_redirect_on_register' );
    $args = array(
      'name' => 'alkitab_redirect_on_register',
      'show_option_none' => 'None',
      'selected' => $selected
    );

    wp_dropdown_pages( $args );
  }

  public function login_page_image_field() {
    $image_id = get_option( 'alkitab_login_image' );
    if ( intval($image_id) > 0 ) {
      $image = wp_get_attachment_image( $image_id, 'medium', false, array( 'id' => 'alkitab_login_image_preview' ) );
    } else {
      $image = '<span id="alkitab_login_image_preview">not selected</span>';
    }

    echo $image;
    echo "<input type='hidden' id='alkitab_login_image' name='alkitab_login_image' value='$image_id'>";
    echo "<br><button id='alkitab_image_manager' >" . __( 'Select image', 'alkitab-users' ) . "</button>";
  }

  public function login_page_image_full_field() {
    $full = boolval(get_option( 'alkitab_login_image_full', false ));
    echo "<input type='checkbox' name='alkitab_login_image_full' value='1'" . checked( 1, $full, false ) . ">";
  }

  public function register_first_title() {
    $title = get_option( 'alkitab_register_first_title' );
    echo "<input name='alkitab_register_first_title' value='$title'>";
  }

  public function register_second_title() {
    $title = get_option( 'alkitab_register_second_title' );
    echo "<input name='alkitab_register_second_title' value='$title'>";
  }

  public function register_third_title() {
    $title = get_option( 'alkitab_register_third_title' );
    echo "<input name='alkitab_register_third_title' value='$title'>";
  }
}