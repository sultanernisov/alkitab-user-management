<?php

namespace AlkitabUsers\API;

class Languages {
  public static function get_languages() {
    if ( array_key_exists( 'wpml_active_languages', $GLOBALS['wp_filter'] ) ) {
      $languages = array();
      $wpml_languages = apply_filters( 'wpml_active_languages' );

      foreach ( $wpml_languages as $language ) {
        $native_name = $language['native_name'];
        $translated_name = $language['translated_name'];

        $languages[] = array(
          'code' => $language['language_code'],
          'label' => "$native_name ($translated_name)",
          'flag_url' => $language['country_flag_url']
        );
      }

      return $languages;
    }

    return [];
  }
}