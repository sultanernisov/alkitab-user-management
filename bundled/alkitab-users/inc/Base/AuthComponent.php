<?php

namespace AlkitabUsers\Base;

class AuthComponent extends RenderComponent {
  protected function redirect_logged_in_user( $redirect_to = null ) {
    $user = wp_get_current_user();

    if ( user_can( $user, 'manage_options' ) ) {
      if ( $redirect_to ) {
        wp_safe_redirect( $redirect_to );
        exit;
      } else {
        wp_redirect( admin_url() );
        exit;
      }
    } else if ( get_option( 'alkitab_redirect_on_login' ) ) {
      wp_redirect( get_page_link( 'alkitab_redirect_on_login' ) );
      exit;
    } else {
      wp_redirect( home_url() );
      exit;
    }

  }
}