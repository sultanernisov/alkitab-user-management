<?php

namespace AlkitabUsers\Base;

class Enqueue {

  public function register() {
    add_action( 'wp_enqueue_scripts', array( $this, 'enqueue' ) );
    add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin' ) );
  }

  public function enqueue() {
    wp_enqueue_style( 'alkitab-users', ALKITAB_USERS_PLUGIN_URI . 'assets/css/main.css' );

    wp_register_script( 'alkitab-users', ALKITAB_USERS_PLUGIN_URI . 'assets/js/main.js', array( 'jquery' ), false, true );

    $data = array(
      'registerUrl' => admin_url( 'admin-ajax.php' ),
      'updated' => __( 'Updated', 'alkitab-users' ),
      'submit' => __( 'Submit', 'alkitab-users' ),
    );

    wp_localize_script( 'alkitab-users', 'alkitabUsers', $data );

    wp_enqueue_script( 'alkitab-users' );
  }

  public function enqueue_admin( $page ) {
    wp_enqueue_media();
    wp_register_script( 'alkitab-users-admin', ALKITAB_USERS_PLUGIN_URI . 'assets/js/admin.js', array( 'jquery' ), false, true );

    $data = array(
      'registerUrl' => admin_url( 'admin-ajax.php' ),
      'updated' => __( 'Updated', 'alkitab-users' ),
      'submit' => __( 'Submit', 'alkitab-users' ),
    );

    wp_localize_script( 'alkitab-users-admin', 'alkitabUsers', $data );

    wp_enqueue_script( 'alkitab-users-admin' );
  }

}