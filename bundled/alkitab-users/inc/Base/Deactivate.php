<?php

namespace AlkitabUsers\Base;

class Deactivate {
  public static function deactivate() {
    Roles::unregister();
    flush_rewrite_rules();
  }
}