<?php

/*
  Plugin Name: Alkitab User Management
  Description: Plugin adds student and tutor roles. Implements custom login and register forms, including profile and etc.
  Version: 0.0.2
  Text Domain: alkitab-users
*/

if ( !defined( 'ABSPATH' ) ) die;

if ( file_exists( dirname( __FILE__ ) . '/vendor/autoload.php' ) ) {
  require_once dirname( __FILE__ ) . '/vendor/autoload.php';
}

define( 'ALKITAB_USERS_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'ALKITAB_USERS_PLUGIN_URI', plugin_dir_url( __FILE__ ) );
define( 'ALKITAB_USERS_PLUGIN', plugin_basename( __FILE__ ) );

function activate_alkitab_users_plugin() {
  AlkitabUsers\Base\Activate::activate();
}

function deactivate_alkitab_users_plugin() {
  AlkitabUsers\Base\Deactivate::deactivate();
}

register_activation_hook( __FILE__, 'activate_alkitab_users_plugin' );
register_deactivation_hook( __FILE__, 'deactivate_alkitab_users_plugin' );

if ( class_exists( 'AlkitabUsers\\Init' ) ) {
  AlkitabUsers\Init::register_services();
}