<div class="c-register-form" data-multistep-form>

  <form
    class="c-multistep-form"
    action="<?= wp_registration_url(); ?>"
    method="POST"
    data-step-container
  >

    <fieldset data-step="0" class="c-multistep-form--step">
      <h2><?= get_option( 'alkitab_register_first_title', __( 'Step 1', 'alkitab-users' ) ); ?></h2>

      <div class="c-form--field">
        <label for="username" class="c-form--label">
          <?= __( 'username', 'alkitab-users' ); ?>
        </label>
        <input
          autocomplete="off"
          type="text"
          class="c-form--input"
          id="username"
          name="username"
        >
      </div>

      <div class="c-form--field">
        <label for="firstName" class="c-form--label">
          <?= __( 'first name', 'alkitab-users' ); ?>
        </label>
        <input
          autocomplete="off"
          type="text"
          class="c-form--input"
          id="firstName"
          name="first_name"
        >
      </div>

      <div class="c-form--field">
        <label for="lastName" class="c-form--label">
          <?= __( 'last name', 'alkitab-users' ); ?>
        </label>
        <input
          autocomplete="off"
          type="text"
          class="c-form--input"
          id="lastName"
          name="last_name"
        >
      </div>

      <div class="c-form--field">
        <label for="email" class="c-form--label">
          <?= __( 'email address', 'alkitab-users' ); ?>
        </label>
        <input
          autocomplete="off"
          type="email"
          class="c-form--input"
          id="email"
          name="email"
        >
      </div>

      <div class="c-form--field">
        <label for="password" class="c-form--label">
          <?= __( 'password', 'alkitab-users' ); ?>
        </label>
        <input
          autocomplete="off"
          type="password"
          class="c-form--input"
          id="password"
          name="password"
        >
      </div>

    </fieldset>
      
    <fieldset disabled data-step="1" class="c-multistep-form--step">
      <h2><?= get_option( 'alkitab_register_second_title', __( 'Step 2', 'alkitab-users' ) ); ?></h2>

      <div class="c-form--field">
        <label for="birthDate" class="c-form--label">
          <?= __( 'birth date', 'alkitab-users' ); ?>
        </label>
        <input
          type="date"
          class="c-form--input"
          id="birthDate"
          name="birth_date"
        >
      </div>

      <div class="c-form--field">
        <label for="countrySelect" class="c-form--label">
          <?= __( 'country', 'alkitab-users' ); ?>
        </label>
        <input
          class="c-form--input"
          id="countrySelect"
          name="country"
        >
      </div>

      <div class="c-form--field">
        <label for="language" class="c-form--label">
          <?= __( 'language', 'alkitab-users' ); ?>
        </label>
        <select class="c-form--input" name="language" id="language">
          <option value="ru"><?= __( 'Русский', 'alkitab-users' ); ?></option>
          <option value="en"><?= __( 'English', 'alkitab-users' ); ?></option>
          <option value="ky"><?= __( 'Kyrgyz', 'alkitab-users' ); ?></option>
          <option value="kz"><?= __( 'Kazakh', 'alkitab-users' ); ?></option>
          <option value="uz"><?= __( 'Uzbek', 'alkitab-users' ); ?></option>
        </select>
      </div>

    </fieldset>

    <fieldset disabled data-step="2" class="c-multistep-form--step">
      <h3><?= get_option( 'alkitab_register_third_title', __( 'Step 3', 'alkitab-users' ) ); ?></h3>
      <div class="c-tutor-container">

        <?php
          $tutors = get_users( array( 'role' => 'tutor' ) );

          foreach ( $tutors as $tutor ) {
            $avatar_url = get_avatar_url( $tutor->ID );
        ?>
          <div class="c-tutor" data-tutor="<?= $tutor->ID; ?>">
            <img src="<?= $avatar_url; ?>" class="c-tutor--avatar">
            <p><?= $tutor->display_name; ?></p>
            <div class="c-tutor--languages">
              <?php foreach ( get_user_meta( $tutor->ID, 'tutor_languages', true ) as $language ):
                  if ( is_array( $language ) ):
              ?>
                <span class='c-tutor--language'>
                  <img src="<?= $language['flag_url']; ?>" alt="">
                  <span><?= $language['label']; ?></span>
                </span>
              <?php endif; endforeach; ?>
            </div>
          </div>
        <?php
          }
        ?>
      </div>
    </fieldset>

  </form>

  <div class="c-register-form--navigation">
    <button class="nonvisible" data-nav-button="prev">
      <i data-feather="arrow-left"></i>
    </button>
    <div class="c-register-form--navigation--bullets">
      <div class="active" data-bullet="0"></div>
      <div data-bullet="1"></div>
      <div data-bullet="2"></div>
    </div>
    <button data-nav-button="next">
      <i data-feather="arrow-right"></i>
    </button>
    <button class="c-register-form--submit" data-submit>
      <span class="c-register-form--submit-text"><?= __( 'Submit', 'alkitab-users' ); ?></span>
      <div class="la-ball-clip-rotate la-sm aqua-blue hidden">
        <div></div>
      </div>
    </button>
  </div>

</div>