<?php
  function has_password_error( $errors ) {
    $codes = array_column( $errors, 'code' );
    $has_password_error = in_array( 'incorrect_password', $codes ) || in_array( 'empty_password', $codes );
    $no_username_errors = !in_array( 'empty_username', $codes ) && !in_array( 'invalid_username', $codes );
    
    if ( $has_password_error && $no_username_errors ) {
      return true;
    }

    return false;
  }
?>

<div class="c-login">
  <h1><?= __( 'Welcome back!', 'alkitab-users' ); ?></h1>

  <?php if ( $env['lost_password_sent'] ): ?>
    <p><?= __( 'Check your email for a link to reset your password', 'alkitab-users' ); ?></p>
  <?php endif; ?>

  <?php if ( $env['errors'] ): ?>
    <div class="c-form--errors" data-error-message>
      <i class="c-form--errors--close" data-error-close data-feather="x-circle"></i>
      <?php foreach ( $env['errors'] as $error ): ?>
        <p><?= $error['message']; ?></p>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>

  <?php if ( $env['password_updated'] ) : ?>
    <p class="c-form--info">
        <?php _e( 'Your password has been changed. You can sign in now.', 'alkitab-users' ); ?>
    </p>
  <?php endif; ?>

  <form method="POST" action="<?= wp_login_url(); ?>" class="c-login-form c-form">
    <div class="c-form--field">
      <label class="c-form--label" for="user_login">
        <?= __( 'username or email', 'alkitab-users' ); ?>
      </label>
      <input
        <?= (!has_password_error( $env['errors'] )) ? 'data-after-error-focus' : ''; ?>
        data-hide-errors
        autocomplete="off"
        class="c-form--input"
        type="text"
        name="log"
        id="user_login"
        value="<?php
          if ( $env['user'] ) echo $env['user'];
        ?>"
      >
    </div>

    <div class="c-form--field">
      <label class="c-form--label" for="user_pass">
        <?= __( 'password', 'alkitab-users' ); ?>
      </label>
      <input
      <?= (has_password_error( $env['errors'] )) ? 'data-after-error-focus' : ''; ?>
        data-hide-errors
        class="c-form--input"
        type="password"
        name="pwd"
        id="user_pass"
      >
    </div>

    <div class="c-form--checkbox-field">
      <input type="checkbox" name="rememberme" id="rememberme" class="c-form--checkbox" value="forever">
      <label class="c-form--checkbox-label" for="rememberme"><?= __( 'remember me', 'alkitab-users' ); ?></label>
    </div>

    <div class="row wrappable">
      <input class="c-login-form--submit" type="submit" value="<?= __( 'Log in', 'alkitab-users' ); ?>">
      <?php $register_page = get_option( 'alkitab_register_page' ); ?>
      <a
        class="c-login-form--signup"
        href="<?= get_permalink( $register_page); ?>"
      >
        <?= get_the_title( $register_page ); ?>
      </a>
    </div>
  </form>
</div>