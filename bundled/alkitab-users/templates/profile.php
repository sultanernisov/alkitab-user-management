<?php $tutor = $env['tutor']; ?>

<div class="c-profile-page">
  <div class="c-profile-page--aside">
    <img src="<?= $env['avatar_url']; ?>" class="c-profile-page--avatar">
    <div class="c-profile-page--aside--content">
      <p class="c-profile--display-name"><?= $env['display_name']; ?></p>
      <p class="c-profile--username"><?= $env['username']; ?></p>

      <?php global $wp; ?>
      <a
        class="c-profile-page--aside--link"
        href="<?= home_url( add_query_arg( 'action', 'edit', $wp->request ) ); ?>"
      >
        <?= __( 'edit', 'alkitab-users' ); ?>
      </a>
    </div>
  </div>

  <div class="c-profile-page--info">
    <!-- language, country, age, tutor, stats, email -->
    <h3 class="c-profile-page--card-title">
      <?= __( 'Overview', 'alkitab-users' ); ?>
    </h3>

    <div class="c-profile-page--card c-profile-page--stats">
      <div class="c-profile-page--stats-metric">
        <span class="c-profile-page--stats-number">
          <?= $env['courses_count']; ?>
        </span>
        <p><?= __( 'Courses', 'alkitab-users' ); ?></p>
      </div>

      <div class="c-profile-page--stats-metric">
        <span class="c-profile-page--stats-number">
          <?= $env['courses_completed_count']; ?>
        </span>
        <p><?= _x( 'Completed', 'courses', 'alkitab-users' ); ?></p>
      </div>

      <div class="c-profile-page--stats-metric">
        <span class="c-profile-page--stats-number">
          <?= $env['certificates_count']; ?>
        </span>
        <p><?= __( 'Certificates', 'alkitab-users' ); ?></p>
      </div>
    </div>

    <h3 class="c-profile-page--card-title">
      <?= __( 'Your tutor', 'alkitab-users' ); ?>
    </h3>

    <div class="c-profile-page--card c-profile-page--tutor">

      <?php if ( is_object( $tutor ) ): ?>
        <img
          class="c-profile-page--tutor-avatar"
          src="<?= get_avatar_url( $tutor->ID ); ?>"
        >
      <?php endif; ?>

      <div class="c-profile-page--tutor-details">
        <p class="c-profile-page--tutor-name">
          <?= is_object( $tutor ) ? $tutor->display_name : '' ; ?>
        </p>
        <div class="c-profile-page--tutor-languages">

          <?php
            if ( is_object( $tutor ) ):
            $tutor_langs = get_user_meta( $tutor->ID, 'tutor_languages', true );

            foreach ( $tutor_langs as $lang ):
              if ( is_array( $lang ) ):
            ?>

              <div class="c-profile-page--tutor-language">
                <img src="<?= $lang['flag_url']; ?>">
                <span><?= $lang['label']; ?></span>
              </div>

            <?php endif; endforeach; endif; ?>
        </div>
      </div>
    </div>

    <h3 class="c-profile-page--card-title">
      <?= __( 'Basic info', 'alkitab-users' ); ?>
    </h3>
    <div class="c-profile-page--card">

      <div class="c-profile-page--card-field">
        <p class="c-profile-page--card-label">
          <?= __( 'Email address', 'alkitab-users' ); ?>
        </p>
        <p class="c-profile-page--card-value">
          <?= $env['email']; ?>
        </p>
      </div> 

      <div class="c-profile-page--card-field">
        <p class="c-profile-page--card-label">
          <?= __( 'Country', 'alkitab-users' ); ?>
        </p>
        <p class="c-profile-page--card-value">
          <?= $env['country']; ?>
        </p>
      </div>

      <div class="c-profile-page--card-field">
        <p class="c-profile-page--card-label">
          <?= __( 'Language', 'alkitab-users' ); ?>
        </p>
        <p class="c-profile-page--card-value">
          <?= $env['language']; ?>
        </p>
      </div>

      <div class="c-profile-page--card-field">
        <p class="c-profile-page--card-label">
          <?= __( 'Birth date', 'alkitab-users' ); ?>
        </p>
        <p class="c-profile-page--card-value">
          <?= $env['birth_date']; ?>
        </p>
      </div>

    </div>

  </div>
</div>