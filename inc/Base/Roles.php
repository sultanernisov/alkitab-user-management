<?php

namespace AlkitabUsers\Base;

class Roles {

  public function register() {
    self::register_roles();
    add_action( 'edit_user_profile', array( self::class, 'show_tutor_language_form' ), 5 );
    add_action( 'show_user_profile', array( self::class, 'show_tutor_language_form' ), 5 );
    add_action( 'personal_options_update', array( self::class, 'update_tutor_languages' ) );
    add_action( 'edit_user_profile_update', array( self::class, 'update_tutor_languages' ) );
  }

  public static function unregister() {
    self::unregister_roles();
  }

  public static function get_roles() {
    return array(
      array(
        'role' => 'student',
        'display_name' => __( 'Student', 'alkitab-users' ),
        'capabilities' => array(
          'read' => true
        )
      ),
      array(
        'role' => 'tutor',
        'display_name' => __( 'Tutor', 'alkitab-users' ),
        'capabilities' => array(
          'read' => true
        )
      )
    );
  }

  public static function register_roles() {
    foreach ( self::get_roles() as $role ) {
      add_role( $role['role'], $role['display_name'], $role['capabilities'] );
    }
  }

  public static function unregister_roles() {
    foreach ( self::get_roles() as $role ) {
      remove_role( $role['role'] );
    }
  }

  public static function show_tutor_language_form( $user ) {
    if ( !in_array( 'tutor', $user->roles ) ) return;
    require_once ALKITAB_USERS_PLUGIN_PATH . '/templates/tutor_language_form.php';
    return tutor_language_form( $user );
  }

  public static function update_tutor_languages( $user_id ) {
    if ( !current_user_can( 'edit_user', $user_id ) ) return false;
    $languages = array();

    foreach ( $_POST['selected'] as $code ) {
      $languages[$code] = array(
        'code' => $code,
        'label' => $_POST['label'][$code],
        'flag_url' => $_POST['flag_url'][$code]
      );
    }

    return update_user_meta(
      $user_id,
      'tutor_languages',
      $languages
    );
  }

}