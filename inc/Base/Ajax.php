<?php

namespace AlkitabUsers\Base;

class Ajax {
  public function register() {
    add_action( 'wp_ajax_alkitab_login_image', array( $this, 'alkitab_login_image' ) );
  }

  public function alkitab_login_image() {
    if ( isset( $_GET['id'] ) ) {
      $image = wp_get_attachment_image( filter_input( INPUT_GET, 'id', FILTER_VALIDATE_INT ), 'medium', false, array( 'id' => 'alkitab_login_image_preview') );
      $data = array(
        'image' => $image
      );
      wp_send_json( $data );
    }
  }
}