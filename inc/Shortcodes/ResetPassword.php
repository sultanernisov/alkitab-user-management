<?php

namespace AlkitabUsers\Shortcodes;
use \AlkitabUsers\Base\AuthComponent;

class ResetPassword extends AuthComponent {
  private $reset_password_page;

  public function __construct() {
    $this->reset_password_page = get_option( 'alkitab_reset_password_page' );
  }

  public function register() {
    add_shortcode( 'alkitab-reset-password', array( $this, 'render' ) );
    add_action( 'login_form_rp', array( $this, 'redirect_to_custom_password_reset' ) );
    add_action( 'login_form_resetpass', array( $this, 'redirect_to_custom_password_reset' ) );
    add_action( 'login_form_rp', array( $this, 'do_password_reset' ) );
    add_action( 'login_form_resetpass', array( $this, 'do_password_reset' ) );
  }

  public function render( $attributes, $content = null ) {
    $default_atts = array();
    $attributes = shortcode_atts( $default_atts, $attributes );

    if ( is_user_logged_in() ) {
      return __( 'You are already logged in', 'alkitab-users' );
    } else {
      if ( isset( $_REQUEST['login'] ) && isset( $_REQUEST['key'] ) ) {
        $attributes['login'] = $_REQUEST['login'];
        $attributes['key'] = $_REQUEST['key'];

        $errors = array();
        if ( isset( $_REQUEST['error'] ) ) {
          $error_codes = explode( ',', $_REQUEST['error'] );

          foreach ( $error_codes as $error_code ) {
            $errors[] = $this->get_error_message( $error_code );
          }
        }

        $attributes['errors'] = $errors;

        return $this->get_template_html( 'reset_password', $attributes );
      } else {
        return __( 'Invalid password reset link', 'alkitab-users' );
      }
    }
  }

  public function do_password_reset() {
    if ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {
      $rp_key = $_REQUEST['rp_key'];
      $rp_login = $_REQUEST['rp_login'];

      $user = check_password_reset_key( $rp_key, $rp_login );
      if ( !$user || is_wp_error( $user ) ) {
        if ( get_option( 'alkitab_logi_page' ) ) {
          $url = get_page_link( get_option( 'alkitab_login_page' ) );

          if ( $user && $user->get_error_code() === 'expired_key' ) {
            $url = add_query_arg( 'login', 'expiredKey', $url );
            wp_redirect( $url );
          } else {
            $url = add_query_arg( 'login', 'invalidKey', $url );
            wp_redirect( $url );
          }
        }
        exit;
      }

      if ( isset( $_POST['pass1'] ) ) {
        if ( $_POST['pass1'] != $_POST['pass2'] ) {
            // Passwords don't match
            $redirect_url = get_page_link( $this->reset_password_page );

            $redirect_url = add_query_arg( 'key', $rp_key, $redirect_url );
            $redirect_url = add_query_arg( 'login', $rp_login, $redirect_url );
            $redirect_url = add_query_arg( 'error', 'password_reset_mismatch', $redirect_url );

            wp_redirect( $redirect_url );
            exit;
        }

        if ( empty( $_POST['pass1'] ) ) {
          // Password is empty
          $redirect_url = get_page_link( $this->reset_password_page );

          $redirect_url = add_query_arg( 'key', $rp_key, $redirect_url );
          $redirect_url = add_query_arg( 'login', $rp_login, $redirect_url );
          $redirect_url = add_query_arg( 'error', 'password_reset_empty', $redirect_url );

          wp_redirect( $redirect_url );
          exit;
        }

        // Parameter checks OK, reset password
        reset_password( $user, $_POST['pass1'] );
        if ( get_option( 'alkitab_login_page' ) ) {
          $login_url = get_page_link( get_option( 'alkitab_login_page' ) );
          $login_url = add_query_arg( 'password', 'changed', $login_url );
          wp_redirect( $login_url );
        } else {
          wp_redirect( wp_login_url() );
        }
      } else {
        echo "Invalid request.";
      }
      exit;
    }
  }

  private function get_error_message( $error_code ) {
    switch ( $error_code ) {
      case 'expiredkey':
      case 'invalidkey':
          return __( 'The password reset link you used is not valid anymore.', 'alkitab-users' );
      
      case 'password_reset_mismatch':
          return __( "The two passwords you entered don't match.", 'alkitab-users' );
          
      case 'password_reset_empty':
          return __( "Sorry, we don't accept empty passwords.", 'alkitab-users' );
      default:
        return;
    }
  }

  public function redirect_to_custom_password_reset() {
    if ( 'GET' == $_SERVER['REQUEST_METHOD'] ) {
      $user = check_password_reset_key( $_REQUEST['key'], $_REQUEST['login'] );
      if ( !$user || is_wp_error( $user ) ) {
        if ( get_option( 'akitab_login_page' ) ) {
          $url = get_page_link( get_option( 'alkitab_login_page' ) );

          if ( $user && $user->get_error_code() === 'expired_key' ) {
            $url = add_query_arg( 'login', 'expiredKey', $url );
            wp_redirect( $url );
          } else {
            $url = add_query_arg( 'login', 'invalidKey', $url );
            wp_redirect( $url );
          }
          exit;
        }
      }

      if ( isset( $this->reset_password_page ) ) {
        $redirect_url = get_page_link( $this->reset_password_page );
        $redirect_url = add_query_arg( 'login', esc_attr( $_REQUEST['login'] ), $redirect_url );
        $redirect_url = add_query_arg( 'key', esc_attr( $_REQUEST['key'] ), $redirect_url );
        
        wp_redirect( $redirect_url );
        exit;
      }
    }
  }
}