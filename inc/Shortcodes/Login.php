<?php

namespace AlkitabUsers\Shortcodes;
use \AlkitabUsers\Base\AuthComponent;

class Login extends AuthComponent {
  private $login_page;
  private $redirect_page;

  public function __construct() {
    $this->login_page = get_option( 'alkitab_login_page' );
    $this->redirect_page = get_option( 'alkitab_redirect_on_login' );
  }

  public function register() {
    add_shortcode( 'alkitab-login', array( $this, 'render' ) );
    // add_action( 'login_form_login', array( $this, 'redirect_to_custom_login' ) );
    add_filter( 'authenticate', array( $this, 'redirect_on_error' ), 101, 3 );
    add_filter( 'login_redirect', array( $this, 'login_redirect' ), 10, 3 );
  }

  public function render( $attributes, $content = null ) {

    $attributes['redirect_to'] = '';
    if ( isset( $_REQUEST['redirect_to'] ) ) {
      $attributes['redirect'] = wp_validate_redirect( $_REQUEST['redirect_to'], $attributes['redirect_to'] );
    }

    $errors = array();
    if ( isset( $_REQUEST['login'] ) ) {
      $error_codes = explode( ',', $_REQUEST['login'] );
  
      foreach ( $error_codes as $code ) {
          $errors[] = $this->get_error_message( $code );
      }
    }
    $attributes['errors'] = $errors;

    $attributes['user'] = '';
    if ( isset( $_REQUEST['user'] ) ) {
      $attributes['user'] = $_REQUEST['user'];
    }

    $attributes['password_updated'] = isset( $_REQUEST['password'] ) && $_REQUEST['password'] == 'changed';

    // Check if the user just requested a new password 
    $attributes['lost_password_sent'] = isset( $_REQUEST['checkmail'] ) && $_REQUEST['checkmail'] == 'confirm';
    return $this->get_template_html( 'login_form', $attributes );
  }

  public function redirect_to_custom_login() {
    if ( $_SERVER['REQUEST_METHOD'] == 'GET' ) {
      $redirect_to = isset( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : null;

      if ( is_user_logged_in() ) {
        $this->redirect_logged_in_user( $redirect_to );
        exit;
      }

      if ( isset( $this->login_page ) ) {
        $login_url = get_page_link( $this->login_page );
  
        if ( !empty( $redirect_to ) ) {
          $login_url = add_query_arg( 'redirect_to', $redirect_to, $login_url );
        }
  
        wp_redirect( $login_url );
        exit;
      }
    }
  }

  public function redirect_on_error( $user, $username, $password ) {
    if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {

      if ( is_wp_error( $user ) && isset( $this->login_page ) ) {
        $error_codes = join( ',', $user->get_error_codes() );
        $login_url = get_page_link( $this->login_page );
        $login_url = add_query_arg( 'login', $error_codes, $login_url );
        $login_url = add_query_arg( 'user', $username, $login_url );
        wp_redirect( $login_url );
        exit;
      }
    }
    return $user;
  }

  public function login_redirect( $redirect_to, $requested_redirect_to, $user ) {
    $roles = get_userdata($user->ID)->roles;
    if ( in_array( 'student', $roles ) && isset( $this->redirect_page ) ) {
      return get_page_link( $this->redirect_page );
    }

    return $redirect_to;
  }

  private function get_error_message( $error_code ) {
    switch ( $error_code ) {
        case 'empty_username':
            return array(
              'code' => 'empty_username',
              'message' => __( 'You need to enter a username to login.', 'alkitab-users' )
            );
 
        case 'empty_password':
            return array(
              'code' => 'empty_password',
              'message' => __( 'You need to enter a password to login.', 'alkitab-users' )
            );
 
        case 'invalid_email':
        case 'invalid_username':
            return array(
              'code' => 'invalid_username',
              'message' => __(
                  "We don't have any users with that username or email address. Maybe you used a different one when signing up?",
                  'alkitab-users'
              )
            );
 
        case 'incorrect_password':
            $message = __(
              "The password you entered wasn't quite right. <a href='%s'>Have you lost your password?</a>",
              'alkitab-users'
            );

            return array(
              'code' => 'incorrect_password',
              'message' => sprintf( $message, wp_lostpassword_url() )
            );

        default:
            break;
    }
     
    return array(
      'code' => 'unknown',
      'message' => __( 'An unknown error occurred. Please try again later.', 'alkitab-users' )
    );
  }
}