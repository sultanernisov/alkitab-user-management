<?php

namespace AlkitabUsers\Shortcodes;
use \AlkitabUsers\Base\AuthComponent;

class Register extends AuthComponent {
  private $register_page;
  private $redirect_page;

  public function __construct() {
    $this->register_page = get_option( 'alkitab_register_page' );
    $this->redirect_page = get_option( 'alkitab_redirect_on_register' );
  }

  public function register() {
    add_shortcode( 'alkitab-register', array( $this, 'render' ) );
    add_action( 'login_form_register', array( $this, 'redirect_to_custom_register' ) );
    add_action( 'wp_ajax_nopriv_register', array( $this, 'register_user' ) );
  }

  public function render( $attributes, $content = null ) {
    $default_atts = array();
    $attributes = shortcode_atts( $default_atts, $attributes );

    if ( is_user_logged_in() ) {
      return __( 'You are already logged in', 'alkitab-users' );
    }

    if ( !get_option( 'users_can_register' ) ) {
      return __( 'Registering new users is currently not allowed', 'alkitab-users' );
    }

    return $this->get_template_html( 'register_form', $attributes );
  }

  public function redirect_to_custom_register() {
    if ( 'GET' == $_SERVER['REQUEST_METHOD'] ) {
      if ( is_user_logged_in() ) {
        $this->redirect_logged_in_user();
        exit;
      }
      
      if ( isset( $this->register_page ) ) {
        wp_redirect( get_page_link( $this->register_page ) );
        exit;
      }
    }
  }

  public function register_user() {
    $required_fields_error = new \WP_Error();

    if ( !$_POST['username'] ) {
      $required_fields_error->add( 'empty_user_login', __( 'Username is required', 'alkitab-users' ) );
    }

    if ( !$_POST['email'] ) {
      $required_fields_error->add( 'empty_user_email', __( 'Email is required', 'alkitab-users' ) );
    }

    if ( $_POST['email'] && !$this->is_valid_email( $_POST['email'] ) ) {
      $required_fields_error->add( 'invalid_email', __( 'We could not recognize your email.', 'alkitab-users' ) );
    }

    if ( !$_POST['password'] ) {
      $required_fields_error->add( 'empty_password', __( 'Password is required', 'alkitab-users' ) );
    }

    if ( count( $required_fields_error->get_error_codes() ) > 0 ) {
      $codes = $required_fields_error->get_error_codes();
      wp_send_json([
        'error' => $codes,
        'message' => $required_fields_error->get_error_messages(),
        'focus' => $this->get_after_error_focus_element($codes)
      ]);
      exit;
    }

    $userdata = [
      'user_login' => $_POST['username'],
      'user_pass'  => $_POST['password'],
      'user_email' => $_POST['email'],
      'first_name' => $_POST['first_name'],
      'last_name'  => $_POST['last_name'],
      'role'       => 'student'
    ];

    $usermeta = [
      'birth_date' => $_POST['birth_date'],
      'country'    => $_POST['country'],
      'language'   => $_POST['language'],
      'tutor'      => $_POST['tutor']
    ];

    $user_id = wp_insert_user( $userdata );
    if ( is_wp_error( $user_id ) ) {
      $error = $user_id;
      $codes = $error->get_error_codes();

      wp_send_json([
        'error' => $codes,
        'message' => $error->get_error_messages(),
        'focus' => $this->get_after_error_focus_element($codes)
      ]);
    } else {
      $meta_id = add_user_meta( $user_id, 'alkitab_user', $usermeta, true );
      $login = wp_signon([
        'user_login' => $_POST['username'],
        'user_password' => $_POST['password']
      ]);


      if ( is_wp_error( $login ) ) {
        wp_send_json([ 'error' => false, 'redirect' => wp_login_url() ]);
      }

      if ( isset( $this->redirect_page ) ) {
        wp_send_json([ 'error' => false, 'redirect' => get_page_link( $this->redirect_page ) ]);
      } else {
        wp_send_json([ 'error' => false, 'redirect' => home_url() ]);
      }
    }
    die;
  }

  private function is_valid_email( $email ) {
    return filter_var( $email, FILTER_VALIDATE_EMAIL );
  }

  private function get_after_error_focus_element( $codes ) {
    $username_errors = [
      'empty_user_login',
      'user_login_too_long',
      'existing_user_login',
      'invalid_username'
    ];

    $email_errors = [
      'existing_user_email',
      'invalid_email'
    ];

    $password_errors = [
      'empty_password'
    ];

    foreach ( $codes as $code ) {
      if ( in_array( $code, $username_errors ) ) {
        return "username";
      }

      if ( in_array( $code, $email_errors ) ) {
        return "email";
      }

      if ( in_array( $code, $password_errors ) ) {
        return "password";
      }
    }

    return;
  }
}