<?php

namespace AlkitabUsers\Pages;
use \AlkitabUsers\API\Settings;
use \AlkitabUsers\API\Callbacks;

class Admin {

  public $settings;
  public $callbacks;
  public $pages;

  public function __construct() {
    $this->settings = new Settings();
    $this->callbacks = new Callbacks();

    $this->pages = array(
      array(
        'title' => __( 'General Settings', 'alkitab-users' ),
        'menu_title' => 'Alkitab Users',
        'capability' => 'manage_options',
        'menu_slug' => 'alkitab_users',
        'callback' => array( $this->callbacks, 'alkitab_users' ),
        'icon_url' => 'dashicons-groups',
        'position' => 3
      )
    );
  }

  public function register() {
    $this->set_settings();
    $this->set_sections();
    $this->set_fields();

    $this
      ->settings
      ->set_pages( $this->pages )
      ->register();
  }

  public function set_settings() {
    $args = array(
      array(
        'option_group' => 'alkitab_users_group',
        'option_name' => 'alkitab_login_page',
      ),
      array(
        'option_group' => 'alkitab_users_group',
        'option_name' => 'alkitab_register_page',
      ),
      array(
        'option_group' => 'alkitab_users_group',
        'option_name' => 'alkitab_lost_password_page',
      ),
      array(
        'option_group' => 'alkitab_users_group',
        'option_name' => 'alkitab_reset_password_page',
      ),
      array(
        'option_group' => 'alkitab_users_group',
        'option_name' => 'alkitab_redirect_on_login',
      ),
      array(
        'option_group' => 'alkitab_users_group',
        'option_name' => 'alkitab_redirect_on_register',
      ),
      array(
        'option_group' => 'alkitab_users_group',
        'option_name' => 'alkitab_login_image'
      ),
      array(
        'option_group' => 'alkitab_users_group',
        'option_name' => 'alkitab_login_image_full'
      ),
      array(
        'option_group' => 'alkitab_users_group',
        'option_name' => 'alkitab_register_first_title'
      ),
      array(
        'option_group' => 'alkitab_users_group',
        'option_name' => 'alkitab_register_second_title'
      ),
      array(
        'option_group' => 'alkitab_users_group',
        'option_name' => 'alkitab_register_third_title'
      ),
    );

    $this->settings->set_settings( $args );
  }

  public function set_sections() {
    $args = array(
      array(
        'id' => 'alkitab_users_pages',
        'title' => __( 'Pages', 'alkitab-users' ),
        'page' => 'alkitab_users'
      ),
      array(
        'id' => 'alkitab_users_login_page',
        'title' => __( 'Login Page Settings', 'alkitab-users' ),
        'page' => 'alkitab_users'
      ),
      array(
        'id' => 'alkitab_users_register_page',
        'title' => __( 'Register Page Settings', 'alkitab-users' ),
        'page' => 'alkitab_users'
      )
    );

    $this->settings->set_sections( $args );
  }

  public function set_fields() {
    $args = array(
      array(
        'id' => 'alkitab_login_page',
        'title' => __( 'Login Page', 'alkitab-users' ),
        'callback' => array( $this->callbacks, 'login_page_field' ),
        'page' => 'alkitab_users',
        'section' => 'alkitab_users_pages'
      ),
      array(
        'id' => 'alkitab_register_page',
        'page' => 'alkitab_users',
        'section' => 'alkitab_users_pages',
        'title' => __( 'Register Page', 'alkitab-users' ),
        'callback' => array( $this->callbacks, 'register_page_field' ),
      ),
      array(
        'id' => 'alkitab_lost_password_page',
        'page' => 'alkitab_users',
        'section' => 'alkitab_users_pages',
        'title' => __( 'Lost Password Page', 'alkitab-users' ),
        'callback' => array( $this->callbacks, 'lost_password_field' )
      ),
      array(
        'id' => 'alkitab_reset_password_page',
        'page' => 'alkitab_users',
        'section' => 'alkitab_users_pages',
        'title' => __( 'Reset Password Page', 'alkitab-users' ),
        'callback' => array( $this->callbacks, 'reset_password_field' ),
      ),
      array(
        'id' => 'alkitab_redirect_on_login',
        'page' => 'alkitab_users',
        'section' => 'alkitab_users_pages',
        'title' => __( 'Redirect after login', 'alkitab-users' ),
        'callback' => array( $this->callbacks, 'redirect_after_login_field' ),
      ),
      array(
        'id' => 'alkitab_redirect_on_register',
        'page' => 'alkitab_users',
        'section' => 'alkitab_users_pages',
        'title' => __( 'Redirect after register', 'alkitab-users' ),
        'callback' => array( $this->callbacks, 'redirect_after_register_field' )
      ),
      array(
        'id' => 'alkitab_login_image',
        'page' => 'alkitab_users',
        'section' => 'alkitab_users_login_page',
        'title' => __( 'Image on login page', 'alkitab-users' ),
        'callback' => array( $this->callbacks, 'login_page_image_field' ),
      ),
      array(
        'id' => 'alkitab_login_image_full',
        'page' => 'alkitab_users',
        'section' => 'alkitab_users_login_page',
        'title' => __( 'Shoul image be full screen? ', 'alkitab-users' ),
        'callback' => array( $this->callbacks, 'login_page_image_full_field' ),
      ),
      array(
        'id' => 'alkitab_register_first_title',
        'page' => 'alkitab_users',
        'section' => 'alkitab_users_register_page',
        'title' => __( 'First Title', 'alkitab-users' ),
        'callback' => array( $this->callbacks, 'register_first_title' )
      ),
      array(
        'id' => 'alkitab_register_second_title',
        'page' => 'alkitab_users',
        'section' => 'alkitab_users_register_page',
        'title' => __( 'Second Title', 'alkitab-users' ),
        'callback' => array( $this->callbacks, 'register_second_title' )
      ),
      array(
        'id' => 'alkitab_register_third_title',
        'page' => 'alkitab_users',
        'section' => 'alkitab_users_register_page',
        'title' => __( 'Third Title', 'alkitab-users' ),
        'callback' => array( $this->callbacks, 'register_third_title' )
      ),
    );

    $this->settings->set_fields( $args );
  }
}